import 'dart:convert';

import 'package:etitc_voto/src/objects/candidate.dart';
import 'package:http/http.dart';

import 'constants.dart';

class ServerRequest {
  static Map<String, String> _headers = {
    "AUTHETITC": Constants.apiKey,
    "Content-type": "application/json"
  };

  static Future<List<Candidate>> fetchCandidates() async {
    String url = "${Constants.candidatesUrl}";
    List<Candidate> companyClientItems = <Candidate>[];

    Response response = await post(url, headers: _headers);
    int statusCode = response.statusCode;
    String body = response.body;

    if (statusCode == 200) {
      for (Map<String, dynamic> x in jsonDecode(body)) {
        companyClientItems.add(Candidate.fromJson(x));
      }
    } else {
      var decoded = jsonDecode(body);
      companyClientItems.add(Candidate.error(decoded["error"]["details"]));
    }
    return companyClientItems;
  }
}
