import 'package:etitc_voto/src/constants.dart';
import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  String text;
  VoidCallback action;

  MyButton(this.text, this.action);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 45,
      child: RaisedButton(
        onPressed: action,
        child: Text(text),
        color: Constants.primaryDark,
        textColor: Color(0xFFFFFFFF),
      ),
    );
  }
}
