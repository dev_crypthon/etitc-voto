import 'package:etitc_voto/src/components/my_button.dart';
import 'package:etitc_voto/src/components/my_input.dart';
import 'package:etitc_voto/src/pages/vote_page.dart';
import 'package:flutter/material.dart';

class WalletPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ingreso al sistema"),
      ),
      body: Padding(
        padding: EdgeInsets.all(30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            MyInput(true, "Hash"),
            SizedBox(
              height: 15,
            ),
            MyButton(
                "Ingresar",
                () => {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => VotePage()))
                    })
          ],
        ),
      ),
    );
  }
}
