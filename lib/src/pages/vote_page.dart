import 'package:etitc_voto/src/components/circular_progress_indicator.dart';
import 'package:etitc_voto/src/components/my_button.dart';
import 'package:etitc_voto/src/objects/candidate.dart';
import 'package:etitc_voto/src/server_request.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class VotePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Selecciona el candidato"),
      ),
      body: FutureBuilder(
        future: ServerRequest.fetchCandidates(),
        builder: (context, snapshot) {
          List<Candidate> data = snapshot.data;
          if (snapshot.connectionState == ConnectionState.done) {
            //final Candidate candidate = snapshot.data;
            return Container(
              child: ListView.builder(
                padding: EdgeInsets.all(15),
                itemCount: data.length,
                itemBuilder: (context, index) {
                  return Card(
                    child: Container(
                      margin: EdgeInsets.all(10),
                      child: Column(
                        children: [
                          Image.network(data[index].image),
                          Text(data[index].name),
                          Text(data[index].description),
                          MyButton("Votar", () {
                            // TODO: Vote pending
                          })
                        ],
                      ),
                    ),
                  );
                },
              ),
            );
          } else {
            return CircularLoadingIndicator();
          }
        },
      ),
    );
  }
}
