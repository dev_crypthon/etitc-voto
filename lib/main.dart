import 'package:etitc_voto/src/constants.dart';
import 'package:etitc_voto/src/pages/basico_page.dart';
import 'package:etitc_voto/src/pages/botones_page.dart';
import 'package:etitc_voto/src/pages/scroll_page.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Votaciones',
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Constants.primaryColor,
        primaryColorDark: Constants.primaryDark,
        primaryColorLight: Constants.primaryLight,
        accentColor: Constants.accentColor,
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
      ),
      initialRoute: 'scroll',
      routes: {
        'basico': (BuildContext context) => BasicoPage(),
        'scroll': (BuildContext context) => ScrollPage(),
        'botones': (BuildContext context) => BotonesPage(),
      },
    );
  }
}
